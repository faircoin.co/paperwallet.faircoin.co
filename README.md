# FairCoin Paperwallet

Store FairCoin on paperwallets.

[Usage instructions](USAGE.md)

## Developers information

The service is made for/controlled by gitlab runner.

### Usage

Go into gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**

Enter variable name **CMD**

#### CMD - commands

~~~
start        # start container
stop         # stop container
~~~

#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
LH_PORT_paperwallet          # local server port
~~~


## Development <small>( manual usage )</small>

If you want create an instance manual then follow the  instructions.

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env
1. run services by ./control.sh

~~~
chmod +x ./control.sh
./control.sh start
./control.sh stop
~~~
