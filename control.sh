#!/bin/bash
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`

case $1 in
  "start")
      docker-compose down
      docker-compose up -d
    ;;
  "stop")
      docker-compose down
    ;;
esac
